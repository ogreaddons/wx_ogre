/// The main frame class
/** The mainFrame class creates a Frame with a wxOgreScene including a wxOgreview
    on the left and a wxOgreViewEventHandler on the right side.
*/

#ifndef MAINFRAME_H
#define MAINFRAME_H

//(*Headers(HelloWorldFrame)
#include <wx/menu.h>
#include <wx/frame.h>
#include <wx/statusbr.h>

#include "wxOgreScene.h"
#include "wxOgreViewEventHandler.h"
//*)

class mainFrame: public wxFrame
{
public:
    /** Standard Constructor
        @param parent The parent of the mainFrame object
        @param id The id of the mainFrame object */
    mainFrame(wxWindow* parent, wxWindowID id = -1);
    /** Standard Destructor*/
    virtual ~mainFrame();

private:
    ///The main Ogre Scene
    wxOgreScene* mpMainScene;
    ///The wxOgreView object
    wxOgreView* mpMainOgreView;
    ///The wxOgreViewEventHandler object
    wxOgreViewEventHandler* mpSecOgreView;
};

#endif // MAINFRAME_H
